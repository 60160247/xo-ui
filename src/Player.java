
public class Player {
	private char name;
	private int win, lose, draw;

	public Player(char name) {
		this.name = name;
		win = 0;
		draw = 0;
		lose = 0;
	}

	public char getName() {
		return name;
	}

	public int getWin() {
		return win;
	}

	public int getDraw() {
		return draw;
	}

	public int getLose() {
		return lose;
	}

	public void win() {
		win++;
	}

	public void draw() {
		draw++;
	}

	public void lose() {
		lose++;
	}
}
